Feature: Share Product POLO Shirt BEECH to mylife
	In order to share product 
	As an already login member
	I want to share POLO Shirt BEECH to mylife

	Scenario: Share POLO Shirt BEECH to mylife with already login 
		Given I am on POLO Shirt BEECH page
		When I click mylife share link
		Then I can see login popup
		When I fill in username with email "webguru10@hotmail.com"
		And  I fill in password with password "test10"
		And  I click login button
        Then I am on popup share page
        When I fill in subject with "Good product"
        And  I click share button
        Then I can not see popup share page
