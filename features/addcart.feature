
Feature: Add product to cart
 In order to add product to cart
 As a visitor user  I want to add 1 product to cart
 then i can see alert success message and see 1 product in cart

Scenario: add 1 product to cart success by visitor user
Given I am on itruemart.com
When I click on first product in Flash Sale section on itruemart homepage
Then I am on that product's detail page
When I click addcart button on product's detail page
Then I can see alert message "เพิ่มสินค้าลงตะกร้าสำเร็จ" 
And I can see 1 product that i add in addcart
