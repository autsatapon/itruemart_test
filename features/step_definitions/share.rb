﻿Given(/^I am on POLO Shirt BEECH page$/) do
  visit("/product/fcuk-polo-shirt-beech-11408.html")
end

When(/^I click mylife share link$/) do
  page.has_content?("POLO Shirt BEECH")
  page.should have_xpath("//ul/li/a[@class='ml_share']")
  find(:xpath,"//ul/li/a[@class='ml_share']").click
end

Then(/^I can see login popup$/) do
  @login_window = page.driver.browser.window_handles.last
  page.within_window @login_window do
	puts page.current_url
    page.has_content?("กรุณาล็อกอิน")
  end
end

When(/^I fill in username with email "(.*?)"$/) do |arg1|
  page.within_window @login_window do
	puts page.current_url
    fill_in 'email', :with => arg1
  end
end

When(/^I fill in password with password "(.*?)"$/) do |arg1|
  page.within_window @login_window do
	puts page.current_url
    fill_in 'password', :with => arg1
  end
end

When(/^I click login button$/) do
  page.within_window @login_window do
	puts page.current_url
    click_button "ตกลง"
  end
end

Then(/^I am on popup share page$/) do
  page.within_window @login_window do
	puts page.current_url
	page.has_content?("แบ่งปันข้อมูลนี้")
  end
end

When(/^I fill in subject with "(.*?)"$/) do |arg1|
  page.within_window @login_window do
	puts page.current_url
    fill_in 'msg', :with => arg1
  end
end

When(/^I click share button$/) do
  page.within_window @login_window do
	puts page.current_url
    click_button "แบ่งปัน"
  end
end

Then(/^I can not see popup share page$/) do
  pending # express the regexp above with the code you wish you had
end