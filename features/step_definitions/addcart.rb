Given(/^I am on itruemart\.com$/) do
  visit("/")
end

When(/^I click on first product in Flash Sale section on itruemart homepage$/) do
  page.should have_xpath("//div[@class='flash_item']")
  first(:xpath,"//div[@class='flash_item']/a").click
end

Then(/^I am on that product's detail page$/) do
  page.should have_xpath("//div[@class='product_preview']")
  page.should have_xpath("//div/a[@class='add_cart']/img")
end

When(/^I click addcart button on product's detail page$/) do
  find(:xpath,"//div/a[@class='add_cart']").click
end

Then(/^I can see alert message "(.*?)"$/) do |arg1|
  page.should have_xpath("//div[@id='popup_message']")	
  page.has_content?(arg1)
end

Then(/^I can see (\d+) product that i add in addcart$/) do |arg1|
  page.should have_xpath("//ul[@id='product_maincart']/li")
end