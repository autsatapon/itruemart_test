When(/^I Click login$/) do
	page.should have_xpath("//div[@class='login_register']/a")
	first(:xpath,"//div[@class='login_register']/a").click
end

Then(/^I am on itruemart login page$/) do
	page.should have_xpath("//input[@id='login_username']")
end

When(/^I Fill Username "(.*?)"$/) do |arg1|	
	fill_in('login_username',:with => arg1)
end

When(/^I Fill Password "(.*?)"$/) do |arg1|
	fill_in('login_password',:with => arg1)
end

When(/^I click Login button$/) do
	find(:xpath,"//input[@id='btn_login']").click
end

Then(/^I can see my display name "(.*?)"$/) do |arg1|
	page.should have_xpath("//div[@class='truesite_signin']/span")	
	page.has_content?(arg1)	
end